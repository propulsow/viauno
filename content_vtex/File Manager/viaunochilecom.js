var dropDownSizes = {
    "33": {
        "cl": "34",
        "eu": "35"
    },
    "34": {
        "cl": "35",
        "eu": "36"
    },
    "35": {
        "cl": "36",
        "eu": "37"
    },
    "36": {
        "cl": "37",
        "eu": "38"
    },
    "37": {
        "cl": "38",
        "eu": "39"
    },
    "38": {
        "cl": "39",
        "eu": "40"
    },
    "39": {
        "cl": "40",
        "eu": "41"
    },
    "40": {
        "cl": "-",
        "eu": "-"
    },
    "41": {
        "cl": "-",
        "eu": "-"
    },
};

function formatPriceWithoutComma(selector, cloneReplace) {
    $(selector).each(function(index, obj) {
        var priceContainerValue = $(this).html();
        if (priceContainerValue.indexOf(',') > 0) {
            var formattedPriceContainerValue = priceContainerValue.substr(0, priceContainerValue.indexOf(','));
            var selectorClass = $(this).attr('class');
            if (cloneReplace && !$(this).parent().hasClass('cloned')) {
                var priceClone = $(this).clone();
                priceClone.insertAfter($(this)).removeClass(selectorClass).addClass(selectorClass + '-clone');
                priceClone.text(formattedPriceContainerValue);
                $(this).parent().addClass('cloned');
                $(this).remove()
            } else {
                $(this).text(formattedPriceContainerValue)
            }
        }
    })
}

function createAccordionSizes() {
    var accordion = '<div id="accordion" data-accordion><div id="accordionButton" data-control>Seleccione una talla</div><div class="rowsContainer" data-content><div class="accordionRowHeader">' + '<div class="ch">Chile / US</div><div class="br">Brazil</div><div class="eu">EU</div></div></div></div>';
    var buyButton = '<a id="buyForSizes" class="buy-button" href="">Comprar</a>';
    $(buyButton).insertAfter('.tabla-tallas');
    $(accordion).insertAfter('.tabla-tallas');
    $('.descripcion-producto .skuList').each(function() {
        $(this).hide();
        var skuName = $('.nomeSku', this).html();
        var skuRaw = skuName;
        if (skuName.indexOf('Â°') >= 0) {
            skuName = skuName.substr(skuName.indexOf('Âº') - 1, skuName.length)
        }

                var price = $('.preco .valor-por strong', this).html();
        if (price) {
            price = price.substr(0, price.indexOf(','));
            if ($('.descripcion-producto .valor').is(':empty')) {
                var priceHtml = $('.preco .valor-de strong', this).html();
                if (priceHtml) {
                    priceHtml = '<span class="old-price">'+priceHtml+'</span>'+price+'</div>'
                } else {
                    priceHtml = price
                }
                $('.descripcion-producto .valor').html(priceHtml);
            }
        } else {
            price = 'Talla no disponible';
        }
        $(this).attr('id', 'skuName_' + skuName);
        var classes = '';
        if (!$('.buy-button', this).length) {
            classes += 'no-stock '
        }
        if (dropDownSizes[skuName]) {
            var row = '<div class="accordionRow ' + classes + '" data-skuname="' + skuName + '" data-sku="' + skuRaw + '" data-price="' + price + '"><div class="ch">' + dropDownSizes[skuName].cl + '</div><div class="br">' + skuName + '</div><div class="eu">' + dropDownSizes[skuName].eu + '</div></div>';
            $('.rowsContainer').append(row)
        }
    });
    $('.accordionRow').click(function() {
        $('.accordionRow').each(function() {
            $(this).removeClass('selected')
        });
        var $skuNameSelector = $('#skuName_' + $(this).data('skuname'));
        var sizeName = $(this).data('skuname');
        var price = $(this).data('price');
        $('#buyForSizes').show();
        if ($('.descripcion-producto .valor').html() != price) {
            $('.descripcion-producto .valor').html(price)
        }
        if ($('.buy-button', $skuNameSelector).length) {
            var href = $('.buy-button', $skuNameSelector).attr('href');
            $('#buyForSizes').attr('href', href);
            $('.portal-notify-me-ref').parent().hide()
        } else if ($('.portal-notify-me-ref', $skuNameSelector).length) {
            $('#buyForSizes').hide();
            $('.portal-notify-me-ref', $skuNameSelector).parent().show();
            $('.portal-notify-me-ref', $skuNameSelector).siblings().hide();
            $('.portal-notify-me-ref .notifymetitle', $skuNameSelector).html('SOLICITAR TALLA');
            $('.portal-notify-me-ref .sku-notifyme-form p', $skuNameSelector).html('Si tu talla estÃƒÂ¡ agotada, puedes solicitarla aquÃƒÂ­.</br>DÃƒÂ©janos tu email y te enviaremos a la brevedad.</br><span>Busco Talla ' + sizeName + '</span>');
            $('.portal-notify-me-ref .sku-notifyme-form .sku-notifyme-client-name', $skuNameSelector).attr('placeholder', 'Nombre');
            $('.portal-notify-me-ref .sku-notifyme-form .sku-notifyme-client-email', $skuNameSelector).attr('placeholder', '@Email');
            $('.portal-notify-me-ref .sku-notifyme-form .sku-notifyme-button-ok', $skuNameSelector).attr('value', 'Solicitar')
        }
        $(this).addClass('selected')
    });
    $('#accordion').accordion({
        "transitionSpeed": 400
    });
    $('#buyForSizes').click(function(e) {
        if ($(this).attr('href') == "") {
            e.preventDefault();
            if (!$('#accordion').hasClass('open')) {
                $('#accordionButton').click()
            }
        }
    });
    var skuId = $('#___rc-p-id').val();
    vtexjs.catalog.getProductWithVariations(skuId).done(function(product) {
        console.log(product);
        if (product.skus.length) {
            $('.accordionRowHeader').append('<div class="stockColumn">STOCK</div>');
            $(product.skus).each(function() {
                var avQty = this.availablequantity;
                if (avQty == '99999') {
                    avQty = '+15'
                }
                $('*[data-sku="' + this.skuname + '"]').append('<div class="stockColumn">' + avQty + '</div>')
            })
        }
    })
}
var handler = function() {
    if ($('#site-wrapper').hasClass('show-nav')) {
        $('#site-wrapper').removeClass('show-nav')
    }
};

function toggleNav() {
    if ($('#site-wrapper').hasClass('show-nav')) {
        $('#site-wrapper').removeClass('show-nav');
        $('.content-home').unbind("click", handler)
    } else {
        $('#site-wrapper').addClass('show-nav');
        $('.content-home').bind("click", handler)
    }
}

function formatPriceWithoutComma(selector, cloneReplace) {
    $(selector).each(function(index, obj) {
        var priceContainerValue = $(this).html();
        if (priceContainerValue.indexOf(',') > 0) {
            var formattedPriceContainerValue = priceContainerValue.substr(0, priceContainerValue.indexOf(','));
            var selectorClass = $(this).attr('class');
            if (cloneReplace && !$(this).parent().hasClass('cloned')) {
                var priceClone = $(this).clone();
                priceClone.insertAfter($(this)).removeClass(selectorClass).addClass(selectorClass + '-clone');
                priceClone.text(formattedPriceContainerValue);
                $(this).parent().addClass('cloned');
                $(this).remove()
            } else {
                $(this).text(formattedPriceContainerValue)
            }
        }
    })
}
$(document).ready(function() {
    $('#carousel-home .box-banner').each(function() {
        $(this).addClass('carousel-item')
    });
    $('#carousel-home').slick({
        dots: !0,
        infinite: !0,
        speed: 300,
        autoplay: !0,
        autoplaySpeed: 2000,
    });
    $('.header .menu-departamento h3, .header .menu-departamento ul').hover(function() {
        var myClass = $(this).attr("class").replace(/ /g, ".");
        if ($('ul.' + myClass + ' li').length) {
            $('ul.' + myClass).attr('style', 'display:block !important')
        }
    }, function() {
        var myClass = $(this).attr("class").replace(/ /g, ".");
        if ($('ul.' + myClass + ' li').length) {
            $('ul.' + myClass).attr('style', 'display:none !important')
        }
    });
    $('.descripcion-producto .tamanho').click(function() {
        var url = $('.descripcion-producto .tamanho').attr('href');
        $(this).colorbox({
            href: url
        })
    });
    if (!$('.tabla-tallas').is(':empty')) {
        createAccordionSizes()
    }
    $('.btn-open-menu-xs').click(function() {
        toggleNav()
    });
    formatPriceWithoutComma('.skuBestPrice', !1);
    formatPriceWithoutComma('.skuListPrice', !1);
    formatPriceWithoutComma('.total-cart-em', !1);
    formatPriceWithoutComma('.best-price', !1);
    formatPriceWithoutComma('.preco', !1);
    formatPriceWithoutComma('.old-price', !1);
    formatPriceWithoutComma('.oldPrice em', !1);
    formatPriceWithoutComma('.newPrice em', !1);
    formatPriceWithoutComma('.installment em', !1);
    $('.navigation-tabs .menu-departamento ul li a').each(function() {
        var title = $(this).attr('title');
        if (title && $(this).parent('li')) {
            title = title.replace(/\W/g, '');
            $(this).parent('li').addClass(title);
            $(this).addClass(title)
        }
    });
    if ($('.content-institucional #accordion').length) {
        $('.content-institucional #accordion div').hide();
        $('.content-institucional #accordion h3').click(function() {
            $(this).next().fadeToggle()
        })
    }
    $('#menu-mobile .menu-departamento ul').each(function() {
        $(this).hide()
    })
    $('#menu-mobile .menu-departamento h3').click(function(e) {
        e.preventDefault();
        if ($(this).parent().hasClass('brandFilter')) {
            $('.brandFilter ul').slideToggle("slow")
        } else {
            var ulSelector = 'ul.' + $(this).attr("class").replace(' ', '.');
            $(ulSelector).slideToggle(400, function() {})
        }
    })
});
$(document).ajaxStop(function() {
    formatPriceWithoutComma('.skuBestPrice', !1);
    formatPriceWithoutComma('.skuListPrice', !1);
    formatPriceWithoutComma('.total-cart-em', !1);
    formatPriceWithoutComma('.best-price', !1);
    formatPriceWithoutComma('.preco', !1);
    formatPriceWithoutComma('.old-price', !1);
    formatPriceWithoutComma('.oldPrice em', !1);
    formatPriceWithoutComma('.newPrice em', !1);
    formatPriceWithoutComma('.installment em', !1);
    formatPriceWithoutComma('.skuBestInstallmentValue', !1)
});
$('.prateleira .discount').each(function() {
    var conteudo = $(this).text();
    var padrao1 = /([0-9]+),([0-9]+)/;
    var padrao2 = /([0-9]+).([0-9]+)/;
    conteudo = conteudo.replace(padrao1, '$1');
    conteudo = conteudo.replace(padrao2, '$1');
    $(this).html(conteudo);
    if (conteudo == 0) {
        $(this).hide()
    }
});
$('#filtro-tiendas').change(function() {
    var filtro = $(this).attr('value');
    if (filtro == filtro) {
        $('.lojas li').hide();
        $('.lojas li.' + filtro).fadeIn('fast')
    } else if (filtro == 'todos') {
        $('.lojas li.todos').fadeIn('fast')
    }
});
$('.fa-bars').click(function() {
    $(this).toggleClass('fa-times')
})