.flag, p.flag.all {display:none;}
.flag.nuevo{background-image:url('/arquivos/nuevo.png');background-repeat:no-repeat;background-size:cover;font-size:0;height:25px;width:65px;display:block;position:absolute;right:5px;top:5px}
.flag.electricos20-,
.flag.makeup-20-,
.flag.makeup-25-,
.flag.makeup-30-,
p.the-balm-20-,
p.palladio,
p.bodygraphy-25-,
p.kocostar,
p.difeel-2-x-5990,
p.dermactin,
.flag.palladio-3x2,
.flag.face-secret-3x2,
.flag.kocostar-3x2,
.flag.dermactin-3x2,
.flag.tinte-ion-3x2,
.flag.shampoo-acond-3x2,
.flag.gelish-3x2,
.flag.dcto20-body-y-balm,
.flag.dcto30-esmaltes-y-trat-morgant,
.flag.dcto40-todos-packs,
.flag.dcto30-cortadoras,
.flag.dcto30-ardell,
.flag.dcto30-shampoos,
.flag.promo-2°-con-el-70--lakme-collage,
.flag.promo-2°-con-el-70--cepillos-y-peinetas,
.flag.dcto25-planchas,
.flag.dcto30-tinturas,
.flag.promo-2°-con-50--tratamientos,
.flag.promo-2°-con-el-50--california-mango,
.flag.promo-2°-con-el-70--esmaltes-y-tratamientos {display:block;font-size:0;position:absolute;top:5px;left:5px;}
.flag.seis-tintes-xp,.flag.seis-tubos-lakme,.flag.seis-tubos-one-only,.flag.seis-tubos-ion-en-crema,.flag.seis-sham-y-acon-haircare {display:block; font-size: 0;position: absolute;top: 5px; right: 5px;}
.flag.electricos20-:before{content:'20%';font-family:'Share Tech',sans-serif;font-size:1.2rem;line-height:2rem;text-align:center;padding-top:10px;color:#fff;background:#3bd4b8;border-radius:50%;height:50px;width:50px;display:block;}
.flag.makeup-20-:before{content:'20%';font-family:'Share Tech',sans-serif;font-size:1.2rem;line-height:2rem;text-align:center;padding-top:10px;color:#fff;background:#3bd4b8;border-radius:50%;height:50px;width:50px;display:block;}
.flag.makeup-25-:before{content:'25%';font-family:'Share Tech',sans-serif;font-size:1.2rem;line-height:2rem;text-align:center;padding-top:10px;color:#fff;background:#3bd4b8;border-radius:50%;height:50px;width:50px;display:block;}
.flag.makeup-30-:before{content:'30%';font-family:'Share Tech',sans-serif;font-size:1.2rem;line-height:2rem;text-align:center;padding-top:10px;color:#fff;background:#3bd4b8;border-radius:50%;height:50px;width:50px;display:block;}
.flag.destacados{display:block;position:absolute;top:10px;right:10px;border:solid 2px #E62A77;text-transform:uppercase;font-size:.8rem;font-weight:bold;padding:0 7px;color:#E62A77}
.flag.bodygraphy{background-image:url('/arquivos/cruelty-free.png');background-repeat:no-repeat;background-size:cover;font-size:0;height:30px;width:85px;display:block;position:absolute;right:5px;top:5px}
p.the-balm-20-:before{content:'20%\AThe Balm';font-family:'Share Tech',sans-serif;font-size:12px;line-height:1rem;text-align:center;padding-top:6px;color:#fff;background:#f4a0ca;border-radius:50%;height:50px;width:50px;display:block;white-space:pre;}
p.palladio:before{content:'30%\APalladio';font-family:'Share Tech',sans-serif;font-size:12px;line-height:1rem;text-align:center;padding-top:6px;color:#0d0d0d;background:#febc75;border-radius:50%;height:50px;width:50px;display:block;white-space:pre;}
p.bodygraphy-25-:before{content:'25%\A Bodyography';font-family:'Share Tech',sans-serif;font-size:12px;line-height:1rem;text-align:center;padding-top:0;color:#fff;background:#3bd4b8;height:35px;width:70px;display:block;white-space:pre;}
p.flag.all {display: none;}
p.kocostar:before{content:'70% en\A 2ª Compra';font-family:'Share Tech',sans-serif;font-size:12px;line-height:1rem;text-align:center;padding-top:2px;color:#fff;background:#dabe0a;height:35px;width:70px;display:block;white-space:pre;box-shadow:-3px 3px #9b8e0d}
p.difeel-2-x-5990:before{content: '2 x $5.990';font-family:'Share Tech',sans-serif;font-size:12px;line-height:1rem;text-align:center;padding-top:8px;color: #7f5b26;background: #f8e9c2;height:30px;width:70px;display:block;white-space:pre;box-shadow: -3px 5px #c9ac5f;}
p.dermactin:before{content: 'Compra 2';font-family:'Share Tech',sans-serif;font-size:12px;line-height:1rem;text-align:center;padding-top:2px;color:#fff;background: #e77d6b;height:35px;width:70px;display:block;white-space:pre;box-shadow: -3px 3px #8f0000;}
p.dermactin:after{content: 'y el 2° a 50%';font-family:'Share Tech',sans-serif;font-size:12px;line-height:1rem;text-align:center;padding-top: 2px;color:#fff;margin-top: -17px;background: #e77d6b;display:block;box-shadow: -3px 3px #8f0000;}
p.flag.palladio-balm,
p.flag.fantasia20- {display:block;font-size:0;position:absolute;top:5px;left:5px}
p.flag.palladio-balm:before{content:'25% Off';font-family:'Share Tech',sans-serif;font-size:15px;line-height:1rem;text-align:center;padding-top:8px;color:#fff;background:#e97ba1;height:30px;width:70px;display:block;box-shadow:-3px 3px #ba5d7c}
p.flag.fantasia20-:before{content:'20% Off';font-family:'Share Tech',sans-serif;font-size:15px;line-height:1rem;text-align:center;padding-top:8px;color:#fff;background:#FF5722;height:30px;width:70px;display:block;box-shadow:-3px 3px #ba3d34}

/*CONFIGURACIÓN BANDERINES*/
 {display:block; font-size: 0;position: absolute;top: 5px;left: 5px;}
/* BANDERIN 3 X 2 */
.flag.palladio-3x2::before,.flag.face-secret-3x2::before,
.flag.kocostar-3x2::before,.flag.dermactin-3x2::before,
.flag.tinte-ion-3x2::before,.flag.shampoo-acond-3x2::before,
.flag.gelish-3x2::before {content: '3 x 2';font-family: 'Share Tech',sans-serif;font-size: 15px;line-height: 1rem;text-align: center;padding-top: 8px;color: #fff;background: #e97ba1;height: 30px;width: 70px;display: block;box-shadow: -3px 3px #ba5d7c;}
/* BANDERIN 20% OFF */
.flag.dcto20-body-y-balm::before {content: '20% Off';font-family: 'Share Tech',sans-serif;font-size: 15px;line-height: 1rem;text-align: center;padding-top: 8px;color: #fff;background: #ee9c44;height: 30px;width: 70px;display: block;box-shadow: -3px 3px #915c24;}
/* BANDERIN 25% OFF */
.flag.dcto25-planchas::before {content: '25% Off';font-family: 'Share Tech',sans-serif;font-size: 15px;line-height: 1rem;text-align: center;padding-top: 8px;color: #fff;background: #a852a5;height: 30px;width: 70px;display: block;box-shadow: -3px 3px #6b236d;}
/* BANDERIN 30% OFF */
.flag.dcto30-esmaltes-y-trat-morgant::before,.flag.dcto30-cortadoras::before,
.flag.dcto30-ardell::before, .flag.dcto30-shampoos::before, .flag.dcto30-tinturas::before {content: '30% Off';font-family: 'Share Tech',sans-serif;font-size: 15px;line-height: 1rem;text-align: center;padding-top: 8px;color: #fff;background: #03A9F4;height: 30px;width: 70px;display: block;box-shadow: -3px 3px #3F51B5;}
/* BANDERIN 40% OFF */
.flag.dcto40-todos-packs::before {content: '40% Off';font-family: 'Share Tech',sans-serif;font-size: 15px;line-height: 1rem;text-align: center;padding-top: 8px;color: #fff;background: #36c154;height: 30px;width: 70px;display: block;box-shadow: -3px 3px #297b3b;}
/* 50% Off 2a Unidad */
.flag.promo-2°-con-50--tratamientos::before, p.flag.promo-2°-con-el-50--california-mango::before {content: '50% Off \A 2ª Unidad';font-family: 'Share Tech',sans-serif;font-size: 14px;line-height: 1rem;text-align: center;padding-top: 2px;color: #000;background: #e0f43b;height: 35px;width: 70px;white-space: pre;box-shadow: -3px 3px #a2b032;display: flex;justify-content: center;align-items: center;}
/* 70% Off 2a Unidad */
.flag.promo-2°-con-el-70--lakme-collage::before,
.flag.promo-2°-con-el-70--cepillos-y-peinetas::before, .flag.promo-2°-con-el-70--esmaltes-y-tratamientos::before {content: '70% Off \A 2ª Unidad';font-family: 'Share Tech',sans-serif;font-size: 14px;line-height: 1rem;text-align: center;padding-top: 2px;color: #fff;background: #e91e63;height: 35px;width: 70px;white-space: pre;box-shadow: -3px 3px #8e2146;display: flex;justify-content: center;align-items: center;}
/*20% DSCTO AL LLEVAR 6 UNIDADES*/
.flag.seis-tintes-xp::before,
.flag.seis-tubos-lakme::before,
.flag.seis-tubos-one-only::before,
.flag.seis-tubos-ion-en-crema::before,
.flag.seis-sham-y-acon-haircare::before {content: '20% Off en \A 6 Unidades';font-family: 'Share Tech',sans-serif;font-size: 14px;line-height: 1rem;text-align: center;padding-top: 2px;color: #fff;background: #665c84;height: 35px;width: 70px;white-space: pre;box-shadow: -3px 3px #170b39;display: flex;justify-content: center;align-items: center}

/* PAGE PRODUCT */
#product-content .col-12 .product-image .flag.tintes70-{top:20px;left:20px}
#product-content .col-12 .product-image .flag.electricos20-{top:20px;left:20px}
@media (max-width:767px){
	.flag.destacados{top:5px;right:5px;border:solid 2px #E62A77;font-size:.65rem;padding:0 4px;}
	.flag.electricos20-:before{content:'20%';font-size:.8rem;padding-top:2px;border-radius:50%;height:35px;width:35px;display:block;}
	.flag.nuevo{height:20px;width:55px;right:3px;top:3px}
}

@media (max-width:575px){
    p.flag.palladio-balm:before, p.flag.fantasia20-:before, p.flag.beauty15-:before { width: 50px; height: 20px; font-size: 13px; margin-top: -2px; padding-top: 1px; }
}
