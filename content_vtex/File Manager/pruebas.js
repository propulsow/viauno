var ModalInicio = {
    init: function( settings, expiration ) {

        this.config = settings;
        this.setup(this.config, expiration);
    },
    setup: function(mconfig, expiration) {

        if(this.getCookie('iframeVisited') != 'visited'){   
            $.extend(mconfig, {onComplete: function() {ModalInicio.iframeCallBack(expiration)}}); 
            $.colorbox(mconfig);
        }
        $(document).bind('cbox_closed', function(){ModalInicio.setCookie(expiration)});
    },
    iframeCallBack: function(expiration) {     
        $('.cboxIframe').load(function(){
            var iframe = $(this).contents();
            iframe.find('.acceptButton').bind('touchstart click', function(){ModalInicio.setCookie(expiration)});           
        });
    },
    setCookie: function(exdays) { // exdays: expiration days
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = 'iframeVisited' + "=" + 'visited' + ";" + expires + ";path=/";
    },
    getCookie: function(cname) {  // cname: cookie name
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
};

/*INICIO SLIDER PRODUCTOS*/
$(document).ready(function(){    
$("#home-page-cyber .vitrine .prateleira > ul").find('.helperComplement').remove();
    $("#home-page-cyber .vitrine .prateleira > ul").slick({
        infinite: true,
        speed: 1500,
        autoplay: true,
        autoplaySpeed: 1500,
        slidesToShow: 6,
        slidesToScroll: 6,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 6,
                    slidesToScroll: 6,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }
        ]

    });
  });
/* FIN SLIDER PRODUCTOS*/

// En document ready, llamar del siguiente modo
// $( document ).ready(function(){
//     if($("body").attr("class") == "home"){
//         expiration = 1; //dias expiracion cookie
//         ModalInicio.init({  href: "https://viauno.myvtex.com/formulario-respuesta", // pagina donde esta contenido del iframe
//                             iframe: true,  // mostrar como iframe
//                             width: "600",
//                             height: "400", 
//                             maxWidth: "90%", // ancho iframe
//                             maxHeight: "90%" // alto iframe
//                          }, expiration);// tiempo de expiracion en dias.
//        
//     }
// });


